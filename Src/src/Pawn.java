public class Pawn extends Pieces {

    public Pawn(Colour pieceColour, Position position) {
        super(pieceColour, position);
    }

    protected boolean isValidMove(Position position, ChessGameBoard board) {
        int startX = this.getPosition().getX();
        int startY = this.getPosition().getY();
        int endX = position.getX();
        int endY = position.getY();

        int direction = this.getPieceCOLOUR() == Colour.BLANC ? -1 : 1;

        if (startX == endX) {
            if (endY == startY + direction && board.getPiecesAt(position) == null) {
                return true;
            }
            if ((startY == 1 && this.getPieceCOLOUR() == Colour.BLANC || startY == 6 && this.getPieceCOLOUR() == Colour.NOIR) &&
                    endY == startY + 2 * direction && board.getPiecesAt(new Position(startX, startY + direction)) == null &&
                    board.getPiecesAt(position) == null) {
                return true;
            }
        }

        if (Math.abs(startX - endX) == 1 && endY == startY + direction) {
            Pieces targetPiece = board.getPiecesAt(position);
            return targetPiece != null && targetPiece.getPieceCOLOUR() == this.getPieceCOLOUR().getOpposite();
        }

        return false;
    }

    @Override
    public String toString() {
        return this.getPieceCOLOUR() == Colour.BLANC ? "♟" : "♙";
    }
}
