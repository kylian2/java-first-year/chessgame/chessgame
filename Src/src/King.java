public class King extends Pieces{
    public King(Colour pieceColour, Position position) {
        super(pieceColour, position);
    }

    private boolean isPathClear(int startX, int startY, int endX, int endY, ChessGameBoard board) {
        int xDirection = (endX - startX) > 0 ? 1 : -1;
        int yDirection = (endY - startY) > 0 ? 1 : -1;

        int x = startX + xDirection;
        int y = startY + yDirection;

        if (board.getPiecesAt(new Position(x,y)) != null) {
            return false;
        }
        x += xDirection;
        y += yDirection;

        return true;
    }

    @Override
    protected boolean isValidMove(Position newPosition, ChessGameBoard board) {
        int startX = this.getPosition().getX();
        int startY = this.getPosition().getY();
        int endX = newPosition.getX();
        int endY = newPosition.getY();

        if (startX == endX && startY == endY){
            return false;
        }

        if (startX == endX){
            return isPathClear(startX,startY,endX,endY,board);
        }

        if (startY == endY){
            return isPathClear(startX,startY,endX,endY,board);
        }

    }

    public String toString(){
        return this.getPieceCOLOUR() == Colour.BLANC ? "♚" : "♔";
    }
}
