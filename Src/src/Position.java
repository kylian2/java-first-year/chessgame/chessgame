import java.util.Objects;

public class Position {
    private final int x;
    private final int y;

    public Position(int x, int y) throws IllegalArgumentException {
        if(!isPositionCorrect(x, y)) throw new IllegalArgumentException("La position n'est pas valide");
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    private boolean isPositionCorrect(int x, int y) {
        return x <= 7 && x >= 0 && y <= 7 && y >= 0;
    }

    public String toString() {
        return "(" + this.x + "," + this.y + ")";
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return x == position.x && y == position.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}

