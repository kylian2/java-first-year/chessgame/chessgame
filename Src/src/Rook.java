public class Rook extends Pieces {

    public Rook(Colour pieceColour, Position position) {
        super(pieceColour, position);
    }

    protected boolean isValidMove(Position position, ChessGameBoard board) {
        int startX = this.getPosition().getX();
        int startY = this.getPosition().getY();
        int endX = position.getX();
        int endY = position.getY();

        if (startX == endX && startY == endY) {
            return false;
        }

        if (startX == endX) {
            return isPathClear(startX, startY, endX, endY, board);
        }

        if (startY == endY) {
            return isPathClear(startX, startY, endX, endY, board);
        }

        return false;
    }

    private boolean isPathClear(int startX, int startY, int endX, int endY, ChessGameBoard board) {
        if (startX == endX) {
            int direction = startY < endY ? 1 : -1;
            for (int y = startY + direction; y != endY; y += direction) {
                if (board.getPiecesAt(new Position(startX, y)) != null) {
                    return false;
                }
            }
        } else if (startY == endY) {
            int direction = startX < endX ? 1 : -1;
            for (int x = startX + direction; x != endX; x += direction) {
                if (board.getPiecesAt(new Position(x, startY)) != null) {
                    return false;
                }
            }
        }

        Pieces targetPiece = board.getPiecesAt(new Position(endX, endY));
        return targetPiece == null || targetPiece.getPieceCOLOUR() == this.getPieceCOLOUR().getOpposite();
    }

    @Override
    public String toString() {
        return this.getPieceCOLOUR() == Colour.BLANC ? "♜" : "♖";
    }
}
