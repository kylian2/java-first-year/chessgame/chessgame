public class Main {
    public static void main(String[] args) {
        try {

            // Position blanc
            Position position0 = new Position(0, 6);
            Position position2 = new Position(1, 3);
            Position position3 = new Position(7, 7);
            Position position4 = new Position(2, 3);
            Position position5 = new Position(6, 2);
            Position position6 = new Position(3, 4);

            // Test de création d'une instance pion blanc et pion noir
            Pawn pionblanc1 = new Pawn(Colour.BLANC,position0);
            Pawn pionnoir2 = new Pawn(Colour.NOIR,position2);
            Pawn pionnoir3 = new Pawn(Colour.NOIR,position4);

            // Création d'une tour blanche
            Rook rookblanc1 = new Rook(Colour.BLANC,position3);

            //Création d'un fous blanc
            Bishop bishopwhite1 = new Bishop(Colour.BLANC,position5);

            //Création d'un cavalier
            Knight knightwhite1 = new Knight(Colour.BLANC,position6);

            // Création d'un nouveau gameBoard
            ChessGameBoard chessGameBoard = new ChessGameBoard();

            // Ajout des pièces sur le GameBoard
            chessGameBoard.addPiece(pionblanc1);
            chessGameBoard.addPiece(pionnoir2);
            chessGameBoard.addPiece(rookblanc1);
            chessGameBoard.addPiece(pionnoir3);
            chessGameBoard.addPiece(bishopwhite1);
            chessGameBoard.addPiece(knightwhite1);

            // Vision du gameBoard
            System.out.println(chessGameBoard.toString());

            pionnoir3.movePiece(knightwhite1.getPosition(),chessGameBoard);

            // Vision du gameBoard
            System.out.println(chessGameBoard.toString());
        }

        catch(IllegalArgumentException e){
            System.err.println("Entrez une valeur de position valide");
        }
    }
}
