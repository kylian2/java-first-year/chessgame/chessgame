import java.util.ArrayList;
import java.util.List;

public class ChessGameBoard {

    public static final int COLUMN = 8;
    public static final int ROW = 8;
    private static final String EMPTY_CELLS = ".";
    private final List<Pieces> pieces;

    public ChessGameBoard() {
        this.pieces = new ArrayList<>();
    }

    public int getColumn() {
        return COLUMN;
    }

    public int getRow() {
        return ROW;
    }

    public Pieces getPiecesAt(Position position) {
        for (Pieces piece : pieces) {
            if (piece.getPosition().equals(position))
                return piece;
        }
        return null;
    }

    public void addPiece(Pieces piece){
        this.pieces.add(piece);
    }

    public void removePiece(Pieces piece){
        this.pieces.remove(piece);
    }

    public String toString() {
        StringBuilder echiquier = new StringBuilder();
        for (int row = 0; row < ROW; row++) {
            for (int col = 0; col < COLUMN; col++) {
                Pieces piece = this.getPiecesAt(new Position(col, row)); // Inversion des coordonnées
                if (piece == null)
                    echiquier.append(" ").append(EMPTY_CELLS).append(" ");
                else
                    echiquier.append(" ").append(piece.toString()).append(" ");
            }
            echiquier.append("\n");
        }
        return echiquier.toString();
    }
}
