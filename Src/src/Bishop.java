public class Bishop extends Pieces {

    public Bishop(Colour pieceColour, Position position) {
        super(pieceColour, position);
    }

    @Override
    protected boolean isValidMove(Position position, ChessGameBoard board) {
        int startX = this.getPosition().getX();
        int startY = this.getPosition().getY();
        int endX = position.getX();
        int endY = position.getY();

        if (startX == endX && startY == endY) {
            return false;
        }

        if (Math.abs(startX - endX) == Math.abs(startY - endY)) {
            return isPathClear(startX, startY, endX, endY, board);
        }

        return false;
    }

    private boolean isPathClear(int startX, int startY, int endX, int endY, ChessGameBoard board) {
        int xDirection = (endX - startX) > 0 ? 1 : -1;
        int yDirection = (endY - startY) > 0 ? 1 : -1;

        int x = startX + xDirection;
        int y = startY + yDirection;

        while (x != endX && y != endY) {
            if (board.getPiecesAt(new Position(x,y)) != null) {
                return false;
            }
            x += xDirection;
            y += yDirection;
        }

        return true;
    }

    @Override
    public String toString() {
        return this.getPieceCOLOUR() == Colour.BLANC ? "♝" : "♗";
    }
}
