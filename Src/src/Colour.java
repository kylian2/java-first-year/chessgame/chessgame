public enum Colour {
    NOIR,
    BLANC;

    public Colour getOpposite(){
        return this == BLANC ? NOIR : BLANC;
    }
}
