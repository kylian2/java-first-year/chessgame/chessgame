public class Knight extends Pieces {

    public Knight(Colour pieceColour, Position position) {
        super(pieceColour, position);
    }

    @Override
    protected boolean isValidMove(Position newPosition, ChessGameBoard board) {
        int startX = this.getPosition().getX();
        int endX = newPosition.getX();
        int startY = this.getPosition().getY();
        int endY = newPosition.getY();

        if (startX == endX && startY == endY){
            return false;
        }

        int xMove = Math.abs(endX - startX);
        int yMove = Math.abs(endY - startY);

        if ((xMove == 2 && yMove == 1) || (xMove == 1 && yMove == 2)){
            Pieces destinationPiece = board.getPiecesAt(newPosition);
            if (destinationPiece == null || destinationPiece.getPieceCOLOUR() != this.getPieceCOLOUR()){
                return true;
            }
        }

        return false;
    }

    public String toString() {
        return this.getPieceCOLOUR() == Colour.BLANC ? "♞" : "♘";
    }
}
