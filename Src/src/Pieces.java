public abstract class Pieces {

    private Colour pieceColour;
    private Position position;

    public Pieces(Colour pieceColour, Position position) {
        this.pieceColour = pieceColour;
        this.position = position;
    }

    public Colour getPieceCOLOUR() {
        return pieceColour;
    }

    public void setPieceCOLOUR(Colour pieceColour) {
        this.pieceColour = pieceColour;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public void movePiece(Position position, ChessGameBoard board) {
        if (this.isValidMove(position, board)) {
            Pieces targetPiece = board.getPiecesAt(position);
            if (targetPiece != null && targetPiece.getPieceCOLOUR() == this.getPieceCOLOUR().getOpposite()) {
                eatPiece(position, board);
            } else if (targetPiece == null) {
                this.setPosition(position);
            }
        }
    }

    protected abstract boolean isValidMove(Position newPosition, ChessGameBoard board);

    protected void eatPiece(Position position, ChessGameBoard board) {
        Pieces targetPiece = board.getPiecesAt(position);
        if (targetPiece != null && targetPiece.getPieceCOLOUR() != this.getPieceCOLOUR()) {
            board.removePiece(targetPiece);
            this.setPosition(position);
        }
    }

    @Override
    public String toString() {
        return super.toString();
    }
}

